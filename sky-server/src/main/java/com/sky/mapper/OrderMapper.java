package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.dto.GoodsSalesDTO;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.entity.Orders;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Mapper
public interface OrderMapper {
    void insert(Orders orders);
    /**
     * 根据订单号查询订单
     * @param orderNumber
     */
    @Select("select * from orders where number = #{orderNumber}")
    Orders getByNumber(String orderNumber);

    /**
     * 修改订单信息
     * @param orders
     */
    void update(Orders orders);

    @Select("select * from orders where status =#{status} and order_time = #{ordertime}")
    List<Orders> getByStatusAndOrderTime(Integer status, LocalDateTime ordertime);
    @Update("update orders set status =#{orderStatus},pay_status = #{OrderPaidStatus},checkout_time =#{check_out_time} where id =#{id}")
    void updateStatus(Integer orderStatus,Integer OrderPaidStatus,LocalDateTime check_out_time,Long id);

    @Select("select * from orders where id = #{id}")
    Orders getById(Long id);

    Page<Orders> pageQuery(OrdersPageQueryDTO ordersPageQueryDTO);

    @Select("select count(id) from orders where status = #{status}")
    Integer contStatus(Integer status);


    Double sumByMap(Map map);

    Integer countByMap(Map map);

    /*
    * 统计销量排名
    * */
    List<GoodsSalesDTO> getSaleTop(LocalDateTime begin,LocalDateTime end);
}

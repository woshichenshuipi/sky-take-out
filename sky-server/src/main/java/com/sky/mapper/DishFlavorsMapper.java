package com.sky.mapper;

import com.sky.entity.DishFlavor;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DishFlavorsMapper {
    /*
    * 批量插入数控
    * */
    void insertBatch(List<DishFlavor> flavors);


    @Delete("delete from dish_flavor where dish_id = #{dishId}")
    void deleteById(Long dishId);

    void deleteByIds(List<Long> dishids);

    @Select("select * from dish_flavor where dish_id = #{DishId}")
    List<DishFlavor> getByDishId(Long DishId);
}

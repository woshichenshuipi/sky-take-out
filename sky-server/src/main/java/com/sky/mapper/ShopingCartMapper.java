package com.sky.mapper;

import com.sky.entity.ShoppingCart;
import com.sky.service.ShopingCartService;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface ShopingCartMapper {
    List<ShoppingCart> list(ShoppingCart shoppingCart);
    @Update("update shopping_cart set number =#{number} where id = #{id}")
    void updateNumber(ShoppingCart shoppingCart);

    @Insert("insert into shopping_cart(name, image,number, user_id, dish_id, setmeal_id, dish_flavor, amount, create_time)VALUES " +
            "(#{name},#{image},#{number},#{userId},#{dishId},#{setmealId},#{dishFlavor},#{amount},#{createTime})")
    void insert(ShoppingCart shoppingCart);

    @Delete("delete from shopping_cart where user_id =#{currentId} ")
    void deleteByUserId(Long currentId);

    @Delete("delete from shopping_cart where id = #{id}")
    void deleteById(Long id);


    void insertBatch(List<ShoppingCart> shoppingCartList);
}

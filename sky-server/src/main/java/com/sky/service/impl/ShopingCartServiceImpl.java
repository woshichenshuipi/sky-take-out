package com.sky.service.impl;

import com.sky.context.BaseContext;
import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.ShoppingCart;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.mapper.ShopingCartMapper;
import com.sky.service.ShopingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class ShopingCartServiceImpl implements ShopingCartService {
    @Autowired
    private ShopingCartMapper shopingCartMapper;
    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private SetmealMapper setmealMapper;
    @Override
    public void addShopingCart(ShoppingCartDTO shopingCartDTO) {
        //判断当前加入购物车的商品是否存在
        ShoppingCart shoppingCart = new ShoppingCart();
        BeanUtils.copyProperties(shopingCartDTO,shoppingCart);
        Long userId = BaseContext.getCurrentId();
        shoppingCart.setUserId(userId);
        List<ShoppingCart> list = shopingCartMapper.list(shoppingCart);
        if(list!=null&&list.size()>0){
//已经存在，数量加一
            ShoppingCart cart = list.get(0);
            cart.setNumber(cart.getNumber()+1);
            shopingCartMapper.updateNumber(cart);
        }else { //不存在，插入一条购物车数据
            Long dishId = shoppingCart.getDishId();
            if(dishId!=null){//菜品
                Dish dish = dishMapper.getById(dishId);
                shoppingCart.setName(dish.getName());
                shoppingCart.setImage(dish.getImage());
                shoppingCart.setAmount(dish.getPrice());

            }else { Long SetmealId = shopingCartDTO.getSetmealId();
                Long setmealId = shopingCartDTO.getSetmealId();
                Setmeal setmeal = setmealMapper.getById(setmealId);
                shoppingCart.setName(setmeal.getName());
                shoppingCart.setAmount(setmeal.getPrice());
                shoppingCart.setImage(setmeal.getImage());

            }
            shoppingCart.setNumber(1);
            shoppingCart.setCreateTime(LocalDateTime.now());
            shopingCartMapper.insert(shoppingCart);

        }


    }

    @Override
    public List<ShoppingCart> showShoppingCart() {
        Long userId = BaseContext.getCurrentId();
        ShoppingCart build = ShoppingCart.builder().userId(userId).build();
        List<ShoppingCart> list = shopingCartMapper.list(build);
        return list;
    }

    /*
    * 清空购物车
    * */
    public void cleanShoppingCart() {
        Long currentId = BaseContext.getCurrentId();
        shopingCartMapper.deleteByUserId(currentId);
    }

    @Override
    public void subShoppingCart(ShoppingCartDTO shoppingCartDTO) {
        ShoppingCart shoppingCart = new ShoppingCart();
        BeanUtils.copyProperties(shoppingCartDTO,shoppingCart);
        shoppingCart.setUserId(BaseContext.getCurrentId());
        List<ShoppingCart> list = shopingCartMapper.list(shoppingCart);
        if(list!=null&&list.size()>0){
            shoppingCart = list.get(0);
            Integer number = shoppingCart.getNumber();
            if(number==1){
                shopingCartMapper.deleteById(shoppingCart.getId());
            }else{
                shoppingCart.setNumber(shoppingCart.getNumber()-1);
                shopingCartMapper.updateNumber(shoppingCart);
            }
        }
    }
}
